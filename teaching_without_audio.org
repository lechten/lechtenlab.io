# Local IspellDict: en
#+LANGUAGE: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2023 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "~/.emacs.d/oer-reveal-org/config.org"

#+REVEAL_THEME: wwu-ercis
#+REVEAL_EXTRA_CSS: ./reveal.js/dist/theme/oer-reveal.css

# Use different title slide with usage notes.
#+INCLUDE: "~/.emacs.d/oer-reveal-org/course-mlde.org"

# Show note with coursemod plugin automatically.
#+OER_REVEAL_COURSEMOD_CONFIG: coursemod: { enabled: true, shown: false }

# Empty license preamble
#+MACRO: licensepreamble

# Legalese in preamble
#+REVEAL_PREAMBLE: <div class="legalese"><p><a href="/imprint.html">Imprint</a> | <a href="/privacy.html">Privacy Policy</a></p></div>

#+TITLE: Teaching Statement
#+SUBTITLE: https://lechten.gitlab.io/teaching.html

#+AUTHOR: Jens Lechtenbörger
#+REVEAL_ACADEMIC_TITLE: Dr.
#+DATE: Summer Term 2023

* Learning

{{{revealimg("./figures/thenounproject/noun_Learning_1697188.meta","","60vh")}}}

#+begin_notes
A couple of years back I realized that lecturing does not lead to
learning, which is not surprising given our knowledge about learning.
Let’s have a brief look.
#+end_notes

** Brain ≈ Muscle
   - Learning involves brain’s *long term memory*
     {{{reveallicense("./figures/3d-man/brain_teacher.meta",,,t)}}}
   - Long term memory needs *repeated* retrieval and practice
     - Spaced out *over time*
     - Effect: Changes in brain’s *neural connections*
   - (Learning does *not* happen [solely] in lectures)

   #+ATTR_HTML: :class slide-source
   (See cite:Huang2019)

#+begin_notes
Thanks to Nobel laureate Carl Wieman (see cite:Huang2019 for a
discussion with him), I like to think of brains as muscles.

The bullet points here emphasize that learning requires changes in our
long term memory.  Clearly, to remember most of us need repeated
retrieval and practice spaced out over time.  In response, our brains’
proteins change to form new neural connections.  A muscle building up.

While lecturing is a good mental exercise for me, it is unlikely to be
a good one for you...
#+end_notes

** Deliberate Practice
   - Characteristics of *Deliberate Practice* to acquire expert skills
     (cite:Eri08, see also cite:EKT93,SEI14)
     {{{reveallicense("./figures/thenounproject/noun_training_2517572.meta","25rh")}}}
     1. Task with *well-defined goal*
     2. Individual *motivated* to improve
     3. *Feedback* on current performance
     4. Ample opportunities for *repetition* and *gradual refinements*

   #+ATTR_REVEAL: :frag appear
   [[color:gray][(Traditional lecturing is “teaching by telling”, does not share *any* characteristic of Deliberate Practice)]]

#+begin_notes
Here you see characteristics of so-called Deliberate Practice, which
is necessary to acquire expert skills across domains.

<Read them>

Research shows that about 10,000 hours of practice are necessary to
compete internationally in a variety of domains, independently of what
one might think of as “talent”.  Note that  10,000 h @ 40 h/wk @ 50 wk/yr
translate to about 5 years.

Thus, considerable effort is necessary for learning, and this effort
cannot be found in “teaching by telling”.
#+end_notes

** Active Learning
   - *Active Learning* increases student performance in science,
     engineering, and mathematics (cite:FEM+14)
     {{{reveallicense("./figures/thenounproject/noun_experience_1943684.meta","35rh")}}}
     #+ATTR_REVEAL: :frag (appear)
     - Active Learning is an umbrella term for diverse
       interventions
       #+ATTR_REVEAL: :frag (appear)
       - Group problem-solving
       - Worksheets or tutorials completed during class
       - Use of personal response systems with or without peer instruction
       - Studio or workshop course designs
     - Notice: Above interventions share several
       characteristics of Deliberate Practice

* Teaching
{{{reveallicense("./figures/3d-man/white-male-1834099_1920.meta","70rh",,t)}}}

#+begin_notes
To cut a long story short, I was lecturing for years, and students
were generally happy with my lecturing.  When I started to integrate
quizzes with classroom response systems into my courses, though, I
realized that the majority of students did not learn in class what I
was hoping them to learn.  Although students did not appear to be
dissatisfied with this situation, I was frustrated, learned more about
learning, and changed my teaching.

Supported by Stifterverband and the state of North Rhine-Westphalia, I
went for Just-in-Time Teaching (JiTT) cite:Lec17, a special type of
flipped learning.  Since then, I have been experimenting a lot, and
student responses have been mixed.  Some love my approach, others hate
it.  Every term I see superb student performances, making me happy, as
well as students tuning out, leaving me in doubt.  Anyways, I do not
plan to go back to lecturing.

Please be reassured that I value constructive criticism.
Please do not hesitate to provide suggestions that might improve our
shared time here at the university.  Yes, we share time here.
#+end_notes

** Flipped Approaches
   - In-class and at-home events flipped cite:BV13,BRK+21
     #+ATTR_REVEAL: :frag appear
     - Individual computer-based instruction paired with active
       learning in class
       {{{reveallicense("./figures/thenounproject/noun_expert_2586751.meta","25rh")}}}
       - Individual learning shaped by individual background and preferences
       - “Lectures” to discuss questions and work on exercises
   #+ATTR_REVEAL: :frag appear
   - Benefits
     {{{reveallicense("./figures/thenounproject/noun_dialogue_2045420.meta","25rh")}}}
     - Better preparation of in-class meetings
       - Reduced cognitive load during initial instruction
       - Greater cognitive capacity in class
     - Valuable/limited *shared* time is used more *effectively* for
       student-instructor interactions

** Research on Flipped Learning
   :PROPERTIES:
   :CUSTOM_ID: flipped-learning-research
   :END:
   - cite:KHG+22: “Fail, flip, fix, and feed – Rethinking flipped
     learning: A review of meta-analyses and a subsequent
     meta-analysis”
     {{{reveallicense("./figures/thenounproject/noun_mistake_3757166.meta","35rh")}}}
     - Arrange process with Fail, Flip, Fix, and Feed
       - Fail: Let students try to solve novel problems even if they
         fail (before instruction)
     - Let us try this!
#+begin_notes
There is a rich body of research providing scientific evidence for the
benefits of flipped classroom approaches on student learning, of which
one meta-analysis is cited here.  That study suggests a specific
sequence of phases called Fail, Flip, Fix, and Feed.  Importantly, the
first phase, Fail, entails letting students try to solve novel
problems on their own before being instructed.  This is likely to lead
to failing attempts, but activates prior knowledge and helps students
to diagnose their own learning, stimulating meta-cognitive processes.

Let us try this!
#+end_notes

** Notes on Flipping
   - Clearly, flipping requires *your* preparation
     {{{reveallicense("./figures/thenounproject/noun_Productivity_2045422.meta","35rh")}}}
     #+ATTR_REVEAL: :frag (appear)
     - Some students find that after being prepared, they do not need
       our class meetings at all
       - This is fine
       - (In-class discussions might still offer benefits: switch role
         and learn as instructor, identify misconceptions)
     - Other students find that class meetings are a waste of time if
       they are not prepared
       - I agree: If you are not prepared, then it probably makes more
         sense to use the session time for self-study than to attend
         the session

** Notes on Active Learning
   - Quote by author of study on actual learning vs feeling of learning
     cite:D+19:
     - “[[https://www.eurekalert.org/pub_releases/2019-09/hu-lil090519.php][The effort involved in active learning can be misinterpreted as a sign of poor learning. On the other hand, a superstar lecturer can explain things in such a way as to make students feel like they are learning more than they actually are.]]”
   #+ATTR_REVEAL: :frag appear
   - Please, do not misinterpret your efforts as signs of poor learning
     {{{reveallicense("./figures/thenounproject/noun_mistake_3757166.meta","35rh")}}}
     - Please, ask early, in our forum, a shared document, during
       sessions
       - (Q&A on class topics should take place in *shared* spaces, might be
         anonymous; other topics might require private spaces)

* The Beginning
#+begin_leftcol40
{{{reveallicense("./figures/3d-man/steps.meta","50rh",,t)}}}
#+end_leftcol40
#+begin_rightcol60
- Lots of questions
  - Why do you attend sessions?
    - On campus or online?
  - Why would you *like* to come to campus?
  - Is everything fine as it is?
  - How *should* learning at a university look like?
- Please share your answers
  - Maybe anonymously in the shared pad for our first session
#+end_rightcol60

#+INCLUDE: "~/.emacs.d/oer-reveal-org/backmatter.org"
#+INCLUDE: "~/.emacs.d/oer-reveal-org/license-disclaimer-cc-by-sa-4.0.org"
