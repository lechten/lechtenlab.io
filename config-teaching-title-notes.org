# Local IspellDict: en
# SPDX-FileCopyrightText: 2021,2023 Jens Lechtenbörger
#+SPDX-License-Identifier: CC-BY-SA-4.0

[Please follow the link to
[[https://oer.gitlab.io/hints.html][usage hints]] first.
As explained there, presentations such as this one have lots of
non-obvious features, e.g., regarding navigation, search, link
formats, and audio configuration.  In this presentation, slides come
with audio even if the folder icon is not shown.]

Dear students,

my name is Jens Lechtenbörger, and I’m a lecturer at the University of
Münster.

In this presentation I’d like to sketch my approach towards teaching,
which might help to clarify our mutual expectations concerning /my/
teaching and /your/ learning.

In short: I’m here to help and I’m happy to help.  However, despite me
being a “lecturer” this does not mean that I plan to lecture (much).
