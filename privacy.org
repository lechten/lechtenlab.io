# Local IspellDict: en

#+SPDX-FileCopyrightText: 2018-2019 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="index.css" />
#+STARTUP: showeverything
#+AUTHOR: Jens Lechtenbörger
#+TITLE: Privacy Policy for lechten.gitlab.io
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil

#+MACRO: website ~lechten.gitlab.io~

This privacy policy explains the collection of personal data during
your visit of HTML pages (mostly presentations) under the domain {{{website}}}.

First of all, you do not need to expose any personal data to access
{{{website}}}.  Personal data are neither collected nor even asked for.
Furthermore, these pages neither embed ads, nor trackers, nor social
media plugins, and they are served without the use of cookies.
However, most pages are served as HTML presentations by web
servers operated by [[https://about.gitlab.com/][GitLab]],
and your web browser quite likely shares lots of data with GitLab’s
web servers, most notably your IP address but also other technical
information which can be used to uniquely track you.
GitLab explains their use of such data in their
[[https://about.gitlab.com/privacy/][privacy policy]].

Besides, some pages embed external web resources (e.g.,
JavaScript, images, videos), and your browser shares its
characteristics with their origin servers as well.
In particular, if you access a
[[https://github.com/hakimel/reveal.js#multiplexing][multiplexed]]
presentation (most likely via a QR code), your browser needs to
contact servers beyond my control that may track you (~cdn.socket.io~
to download a JavaScript library and
~reveal-js-multiplex-ccjbegmaii.now.sh~ to synchronize master and
client presentations).  You are of course free not to access such
presentations.

In addition, depending on your jurisdiction, your internet service
provider (ISP) may be required by law or misguided by monetary incentives
to record your IP addresses or browsing behavior.  On top of that,
thanks to the
[[https://en.wikipedia.org/wiki/Global_surveillance_disclosures_(2013%E2%80%93present)][Snowden revelations]]
we know that most foreign intelligence services collect all Internet
data that they can acquire, including our browsing behavior.

The good news is that you do not need to be tracked in any of the above
ways if you choose so, in particular under {{{website}}}, as explained subsequently.

First, if you do not want to be tracked when surfing the web in
general, you need to learn and apply techniques for digital
self-defense.  Luckily, appropriate privacy enhancing tools are available as
[[https://fsfe.org/about/basics/freesoftware.en.html][free software]].
Personally, I perform my “random” browsing protected by the
[[https://www.torproject.org/projects/torbrowser.html.en][Tor Browser]],
which I recommend (here
[[https://www.informationelle-selbstbestimmung-im-internet.de/Anonymes_Surfen_mit_Tor.html][in German]],
here [[https://blogs.fsfe.org/jens.lechtenboerger/2015/12/23/you-need-tor-and-tor-is-asking-for-your-support/][in English]]).
The HTML pages served under {{{website}}} are even usable under the
higher-than-default “Safer” security settings in Tor Browser.

Alternatively, you can download all material and use that locally and offline
afterwards.
(As mentioned above, some pages include web resources from
external servers, which will not be available for offline use.
When your browser fetches such resources, those requests can again be
tracked on the web, which is why the Tor Browser might be your first
choice.)
