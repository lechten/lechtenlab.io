<!--- Local IspellDict: en -->
<!--- SPDX-FileCopyrightText: 2019 Jens Lechtenbörger -->
<!--- SPDX-License-Identifier: CC0-1.0 -->

This project creates an [HTML index page](https://lechten.gitlab.io/) for
[selected projects](https://gitlab.com/lechten), in particular, talks
and publications as [Open Educational Resources (OER)](https://en.wikipedia.org/wiki/Open_educational_resources).
