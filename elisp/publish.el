;;; publish.el --- Publish reveal.js presentation from Org files on Gitlab Pages
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2017-2021,2023 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;; Originally inspired by publish.el by Rasmus:
;; https://gitlab.com/pages/org-mode/blob/master/publish.el


;;; Code:
;; Avoid update of emacs-reveal, enable stacktraces.
(setq emacs-reveal-managed-install-p nil
      debug-on-error t)

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir))
(require 'emacs-reveal)

(oer-reveal-publish-all
 (list
  (list "html"
        :base-directory "."
        :include '("index.org" "imprint.org" "privacy.org"  "privacy-de.org")
        :exclude ".*"
        :publishing-function '(oer-reveal-publish-to-html)
        :publishing-directory "./public")
  (list "favicon"
        :base-directory "."
        :include '("favicon.ico" "favicon.png")
        :exclude ".*"
        :publishing-function 'org-publish-attachment
        :publishing-directory "./public")
  (list "robots"
        :base-directory "."
        :include '("robots.txt")
        :exclude ".*"
        :publishing-function 'org-publish-attachment
        :publishing-directory "./public")
  (list "title-logos"
	:base-directory "non-free-logos/title-slide"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/title-slide"
	:publishing-function 'org-publish-attachment)
  (list "theme-logos"
	:base-directory "non-free-logos/reveal-css"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/reveal.js/css/theme"
	:publishing-function 'org-publish-attachment)))
;;; publish.el ends here
